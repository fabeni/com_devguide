<?php

/**
 * @package com_devguide
 * @subpackage site/controller
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Default controller
 */

/**
 * DevguideControllerDefault class.
 */
class DevguideControllerDefault extends JControllerBase
{
    /**
     * @var string $extensionName
     */
    public $extensionName;

    /**
     * @var string $controllerName
     */
    public $controllerName;

    /**
     * @var string $defaultTask
     */
    public $defaultTask = 'index';

    /**
     * @var JModelBase $model
     */
    public $model;

    /**
     * Constructor.
     *
     * @param string $extensionName
     * @param JInput $input
     * @param JApplicationBase $app
     */
    public function __construct($extensionName, JInput $input = null, JApplicationBase $app = null)
    {
        parent::__construct($input, $app);

        $this->extensionName = $extensionName;
        $this->controllerName = 'default';
        $this->model = new DevguideModelPerson();
    }

    /**
     * Executes the controller.
     *
     * @see JController:execute()
     */
    public function execute()
    {
        $taskName = $this->getTaskName();
        if (JDEBUG)
        {
            $controllerClassName = get_class();
            JLog::add("Executing task `$taskName` for the controller `$controllerClassName`", Jlog::INFO, 'controller');
        }

        $taskMethod = $this->getTaskMethod($taskName);
        if (!method_exists($this, $taskMethod))
        {
            throw new Exception(sprintf(
                'Unable to execute the task `%s` on the controller `%s`.',
                $currentTask,
                get_class()
            ), 500);
        }

        echo $this->createView($this->$taskMethod(), $taskName)->render();
    }

    /**
     * Executes the "index" task.
     */
    public function indexTask()
    {
        return array(
            'persons' => $this->model->findAll()
        );
    }

    /**
     * Returns
     * @param array $params
     * @return JViewBase
     */
    private function createView($params, $layout = 'index')
    {
        $viewClassName = $this->getViewClassName();
        if (!class_exists($viewClassName))
        {
            throw new Exception(sprintf(
                'Invalid or unknown view `%s`.',
                $viewClassName
            ), 500);
        }

        if (JDEBUG)
        {
            JLog::add("Creating view `$viewClassName` with layout `$layout`", Jlog::INFO, 'controller');
        }

        $view = new $viewClassName($this->model);
        if (!is_array($params))
        {
            throw new InvalidArgumentException(sprintf(
                'Invalid parameters for view `%s`.',
                $viewClassName
            ), 500);
        }

        foreach ($params as $key => $value)
        {
            $view->$key = $value;
        }

        return $view->setLayout($layout);
    }

    /**
     * Returns the current task's name.
     *
     * @return string
     */
    private function getTaskName()
    {
        return $this->input->getCmd('task', $this->defaultTask);
    }

    /**
     * Returns the full task's method name.
     *
     * @return string
     */
    private function getTaskMethod($taskName)
    {
        return sprintf('%sTask', $taskName);
    }

    /**
     * Returns the view's class name.
     *
     * @return string
     */
    private function getViewClassName()
    {
        $format = $this->input->getCmd('format', 'html');
        return sprintf('%sView%s%s', ucfirst($this->extensionName), ucfirst($this->controllerName), ucfirst($format));
    }
}
