<?php

/**
 * @package com_devguide
 * @subpackage site/controller
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Default controller
 */

/**
 * DevguideControllerDispatcher class.
 */
class DevguideControllerDispatcher extends JControllerBase
{
    /**
     * @var string $extensionName
     */
    public $extensionName;

    /**
     * @var string $defaultControllerName
     */
    public $defaultControllerName;

    /**
     * Constructor.
     *
     * @param string $extensionName
     * @param JInput $input
     * @param JApplicationBase $app
     */
    public function __construct($extensionName, $defaultControllerName = 'default', JInput $input = null, JApplicationBase $app = null)
    {
        parent::__construct($input, $app);

        JLog::addLogger(array('text_file' => $extensionName.'.log.php'), JLog::ALL);

        $this->extensionName = $extensionName;
        $this->defaultControllerName = $defaultControllerName;

		$this->app = isset($app) ? $app : JFactory::getApplication();
		$this->input = isset($input) ? $input : $this->app->input;
    }

    /**
     * Executes the controller.
     *
     * @see JController:execute()
     */
    public function execute()
    {
        $controllerClassName = $this->getControllerClassName();
        if (JDEBUG)
        {
            JLog::add("Dispatching request to the controller `$controllerClassName`", Jlog::INFO, 'dispatcher');
        }

        if (!class_exists($controllerClassName))
        {
            throw new InvalidArgumentException(sprintf(
                'Invalid or unknown controller `%s`.',
                $controllerClassName
            ), 404);
        }

        $controllerInstance = new $controllerClassName($this->extensionName, $this->input, $this->app);
        $controllerInstance->execute();
    }

    /**
     * Returns the controller's class name.
     *
     * @return string
     */
    protected function getControllerClassName()
    {
        return sprintf('%sController%s',
            ucfirst($this->extensionName),
            ucfirst($this->input->get('controller', $this->defaultControllerName))
        );
    }
}
