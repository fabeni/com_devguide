<?php

/**
 * @package com_devguide
 * @subpackage site
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Site entry point
 */

defined('_JEXEC') or die;

JLoader::registerPrefix('Devguide', JPATH_SITE . '/components/com_devguide');

$dispatcherInstance = new DevguideControllerDispatcher('devguide');
$dispatcherInstance->execute();
