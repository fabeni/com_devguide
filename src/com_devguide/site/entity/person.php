<?php

/**
 * @package com_devguide
 * @subpackage site/entity
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Person entity
 */

defined('_JEXEC') or die;

/**
 * DevguideEntityPerson class.
 */
class DevguideEntityPerson
{
    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var string $surname
     */
    public $surname;

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->name, $this->surname);
    }
}
