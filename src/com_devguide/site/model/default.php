<?php

/**
 * @package com_devguide
 * @subpackage site/model
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Default model
 */

defined('_JEXEC') or die;

/**
 * DevguideModelDefault class.
 */
class DevguideModelDefault extends JModelBase
{

}
