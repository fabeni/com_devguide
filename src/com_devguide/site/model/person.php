<?php

/**
 * @package com_devguide
 * @subpackage site/model
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description Default model
 */

defined('_JEXEC') or die;

/**
 * DevguideModelPerson class.
 */
class DevguideModelPerson extends JModelDatabase
{
    /**
     * @var string $entityClassName
     */
    private $entityClassName = 'DevguideEntityPerson';

    /**
     * @var string $tableName
     */
    private $tableName = '#__devguide_person';

    /**
     * @return array
     */
    public function findAll()
    {
        $query = $this->buildBaseQuery()->order("surname");

        return $this->getDb()->setQuery($query)->loadObjectList('id', $this->entityClassName);
    }

    /**
     * @param integer $id
     * @return DevguideEntityPerson
     */
    public function find($id)
    {
        $query = $this->buildBaseQuery()->where("id = $id");

        return $this->getDb()->setQuery($query)->loadObject($this->entityClassName);
    }

    /**
     * @return JDatabaseQuery
     */
    protected function buildBaseQuery()
    {
        return $this->getDb()->getQuery(true)->select('*')->from($this->tableName);
    }
}
