<?php

/**
 * @package com_devguide
 * @subpackage view/default
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description "Default" view
 */

defined('_JEXEC') or die;

/**
 * DevguideViewDefaultHtml class.
 */
class DevguideViewDefaultHtml extends JViewHtml
{

    /**
     * Constructor.
     *
     * @param JModel $model
     * @param SplPriorityQueue $paths
     */
    public function __construct(JModel $model, SplPriorityQueue $paths = null)
    {
        parent::__construct($model, $paths);

        $this->paths->insert(JPATH_SITE . '/components/com_devguide/view/default/tmpl', 1);
    }
}
