<?php

/**
 * @package com_devguide
 * @subpackage view/default/tmpl
 * @author Ingannatore
 * @authorUrl https://bitbucket.org/Ingannatore
 * @copyright Copyright (C) 2013 Matteo Guindani
 * @license GNU/GPLv2
 * @description "Index" layout for the "Default" view
 */

defined('_JEXEC') or die;

?>
<h1>com_devguide</h1>
<table>
    <tbody>
        <?php foreach ($this->persons as $person): ?>
        <tr>
            <td><?php echo $person->surname ?></td>
            <td><?php echo $person->name ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>